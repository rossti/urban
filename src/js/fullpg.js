$(function() {
    var slickInChange = false;
    var onChangeSlide = null;
    var currentPage = 1;
    var pageSlides = {
        1: {
            from: 0,
            to: 0
        },
        2: {
            from: 1,
            to: 3
        },
        3: {
            from: 4,
            to: 4
        },
        4: {
            from: 5,
            to: 6
        },
        5: {
            from: 7,
            to: 8
        },
        6: {
            from: 9,
            to: 11
        }
    }

    $('.iphone-slider')
        .on('beforeChange', function(event, slick, currentSlide, nextSlide){
            slickInChange = true;
        })
        .on('afterChange', function(event, slick, currentSlide, nextSlide){
            slickInChange = false;
            if (onChangeSlide) {
                onChangeSlide();
            }
            if (currentSlide === pageSlides[currentPage].from && slick.direction === 0) {
                slick.direction = slick.direction ? 0 : 1;
            }
            if(currentSlide === pageSlides[currentPage].to && slick.direction === 1) {
                slick.direction = slick.direction ? 0 : 1;
            }
    });


    window.addEventListener("orientationchange", function() {
        window.setTimeout('location.reload()', 200); //Reloads after 0.2s
        console.log('сменил');
    }, false);//--- reload page after orientaton change


    if ($(window).width() < 476) {
        $('.counter-numbers').countTo();
    } else {
        $('#fullpage').fullpage({
            scrollingSpeed: 700,
            fitToSectionDelay: 300,
            navigation: true,
            navigationPosition: 'right',
            onLeave: function(index, nextIndex, direction) {

                $('.animFirstLeft, .animSecondLeft').removeClass('animated fadeInLeft');
                $('.animFirstRight, .animSecondRight').removeClass('animated fadeInRight');
                $('.aside-description__left').removeClass('animated fadeInLeft');
                $('.aside-description__right').removeClass('animated fadeInRight');
                $('.aside-description__left').removeClass('animated fadeInDown');
                $('.aside-description__right').removeClass('animated fadeInDown');
                $('.mark').removeClass('animated fadeInDown');
                $('.section5-animation').removeClass('animated fadeInUpBig');
                $('.animation-row').removeClass('animated fadeInUpBig');
                $('.section8 .animation-row').removeClass('animated fadeInUpBig');
                $('.up').removeClass('animated fadeInDownBig');
                $('.slow').removeClass('animated fadeInUpBig');

                if
                (
                    ((index == 1)||(index == 2)||(index == 3)||(index == 4)||(index == 5)||(index == 6)||(index == 7)||(index == 8)||(index == 9)||(index == 10)) &&
                    ((nextIndex == 7)||(nextIndex == 8)||(nextIndex == 9)||(nextIndex == 10))
                ) {
                    $('.fixed-iphone').removeClass('animated fadeIn');
                    $('.fixed-iphone').addClass('animated zoomOut').css('opacity','0');
                    setTimeout(function () {
                        $('.fixed-iphone').css('display','none');
                    },500);

                    if (nextIndex == 7) {
                         $('.counter-numbers').countTo()
                    }
                    if (nextIndex == 8) {
                        $('.section8 .animation-row').addClass('animated fadeInUpBig');
                    }
                    if (nextIndex == 9) {
                        $('.up').addClass('animated fadeInDownBig');
                        $('.slow').addClass('animated fadeInUpBig');
                    }

                } else  {
                    if (
                        ((index == 7)||(index == 8)||(index == 9)||(index == 10)) && ((nextIndex==1)||(nextIndex==2)||(nextIndex==3)||(nextIndex==4)||(nextIndex==5)||(nextIndex==6))
                    ) {
                        setTimeout(function () {
                            $('.fixed-iphone').removeClass('animated zoomOut');
                            $('.fixed-iphone').css('display','block').addClass('animated fadeIn').css('opacity','1').css('animation-duration','1s');
                        }, 500)
                    }


                    if (nextIndex == 1) {
                        $('.animFirstLeft, .animSecondLeft').addClass('animated fadeInLeft');
                        $('.animFirstRight, .animSecondRight').addClass('animated fadeInRight');
                    }
                    if (nextIndex == 2) {
                        $('.aside-description__left').addClass('animated fadeInLeft');
                        $('.aside-description__right').addClass('animated fadeInRight');
                        $('.mark').addClass('animated fadeInDown');
                    }
                    if (nextIndex == 3) {
                        $('.aside-description__left').addClass('animated fadeInDown');
                        $('.aside-description__right').addClass('animated fadeInDown');
                    }
                    if (nextIndex == 4) {
                        $('.aside-description__left').addClass('animated fadeInLeft');
                        $('.aside-description__right').addClass('animated fadeInRight');
                    }
                    if (nextIndex==5) {
                        $('.section5-animation').addClass('animated fadeInUpBig');
                    }
                    if (nextIndex==6) {
                        $('.animation-row').addClass('animated fadeInUpBig');
                    }
                    var direction = index < nextIndex ? 1 : 0;
                    currentPage = nextIndex;
                    if (pageSlides[currentPage].from === pageSlides[currentPage].to) {
                        $('.iphone-slider').slick('slickPause');
                    } else {
                        $('.iphone-slider').slick('slickPlay');
                    }

                    if (!slickInChange) {
                        // $('.iphone-slider')[0].slick.direction = direction;
                        $('.iphone-slider')
                            .slick('slickGoTo', pageSlides[currentPage].from);
                    } else {
                        onChangeSlide = (function (currentPage, direction) {
                            return function () {
                                setTimeout(function () {
                                    // $('.iphone-slider')[0].slick.direction = direction;
                                    $('.iphone-slider')
                                        .slick('slickGoTo', pageSlides[currentPage].from);
                                    onChangeSlide = null;
                                });
                            };
                        })(currentPage, direction);
                    }

                }
            },
            afterLoad: function () {
                $('.animFirstLeft, .animSecondLeft').addClass('animated fadeInLeft');
                $('.animFirstRight, .animSecondRight').addClass('animated fadeInRight');
            }
        });
    }

});