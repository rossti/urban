$(function(){

    //---burger
    $('#mobile-nav-btn').click(function(){
        $(this).toggleClass('open');
        $('.mobile-navigation').toggleClass('hide-nav');
        $('.animation-left').toggleClass('animated fadeInLeft');
    });
    $('.animation-left').click(function () {
        $('.mobile-navigation').toggleClass('hide-nav');
        $('#mobile-nav-btn').toggleClass('open');
    });
    //---burger

    //---countdown
    var clock = $('#clock').FlipClock({
        clockFace: 'DailyCounter',
        autostart: true
    });
    var dt = 'June 15 2017 19:00:00';
    var first = new Date(dt);
    var last = Date.now();
    var remaining = first - last;
    remaining /= 1000;
    clock.setTime(remaining);
    clock.setCountdown(true);
    clock.start();
    var countup = setInterval(function() {
        if(clock.getTime().time < 1) {
            clock.stop();
            $('#clock').css('display','none');
            $('.section1 .before-start__description').css('display','none');
            $('.section1 .before-start__timer').text('Приложение запущено!');
            clearInterval(countup);
        }
    },500);
    //---countdown

    //---iphone slider
    if ($(window).width() < 476) {
        $('#link1').attr('href','#section1');
        $('#link2').attr('href','#section2');
        $('#link3').attr('href','#section3');
        $('#link4').attr('href','#section4');
        $('#link5').attr('href','#section5');
        $('#link6').attr('href','#section6');
        $('#link7').attr('href','#section7');
        $('#link8').attr('href','#section8');
        $('#link9').attr('href','#section9');
        $('#link10').attr('href','#section10');
        $('.iphone-mobileS').slick({
            slideToShow: 1,
            slideToScroll: 1,
            arrows: false,
            dots: false,
            autoplay: true,
            autoplaySpeed: 3000,
            infinite: true,
            vertical: true,
            draggable: false,
            pauseOnFocus: false,
            pauseOnHover: false,
            swipe: false
        });
    } else {
        $('.iphone-slider').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: false,
            arrows: false,
            speed: 600,
            autoplaySpeed: 3000,
            vertical: true,
            pauseOnFocus: false,
            infinite: false,
            waitForAnimate: false,
            draggable: false,
            pauseOnHover: false,
            swipe: false
        });
    }

    //---iphone slider

    //---leader type filter

    $('#type-step').click(function(){
        $('.type-btn').removeClass('active-type');
        $(this).addClass('active-type');
        $('#step-leaders').addClass('visible-leaders');
        $('#coin-leaders').removeClass('visible-leaders');
    });

    $('#type-money').click(function(){
        $('.type-btn').removeClass('active-type');
        $(this).addClass('active-type');
        $('#coin-leaders').addClass('visible-leaders');
        $('#step-leaders').removeClass('visible-leaders');
    });
    //---leader type filter

    //---partners slider
    if ($('.trademarks__line .trademarks-one').length > 4) {
        $('.trademarks__line').slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            autoplay: true,
            arrows: false,
            dots: false,
            speed: 600,
            autoplaySpeed: 3000,
            infinite: true,
            draggable: false,
            responsive: [
                {
                    breakpoint: 1025,
                    settings: {
                        slidesToShow: 3
                    }
                },
                {
                    breakpoint: 476,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                }
            ]
        })
    }
    //---partners slider
});