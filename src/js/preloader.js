$(document).ready(function () {
    $(window).on('load', function () {
        var $preloader = $('#preloader'),
            $svg_anm   = $preloader.find('.prelogo');
        $svg_anm.fadeOut();
        $preloader.delay(500).fadeOut('slow');
    });
});